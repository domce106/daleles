import random_gauss
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.mlab as mlab
n=20000
higgs=[]
for x in range(n):
    higgs.append(random_gauss.rand_gauss(125,0.004))
wh = np.ones_like(higgs)/float(len(higgs))
n, bins, patches = plt.hist(higgs, 70, weights=wh, edgecolor='black')
plt.xlabel('Energy')
plt.ylabel('Probability')
plt.title('Higgs boson energy')
plt.show()
