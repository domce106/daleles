import math
import numpy as np

def mult (X, Y):

    Xrow = len(X)
    Xcol = len(X[0])
    Yrow = len(Y)
    Ycol = len(Y[0])
    Z = [[0 for j in range(Xrow)] for l in range(Ycol)]
    if Xrow != Yrow:
        print("matricos blogos...")
        quit()

    for i in range(Xrow):
        for p in range(Xcol):
            hold1 = 0
            for k in range(Xrow):
                a = X[i][k]
                b = Y[k][p]
                hold1 = hold1 + a*b
            Z[i][p] = hold1
    return Z

def lorentz(p4, vvec, meter):  #p4:[mass, px, py, pz]     vvec:[mass, pxframe, pyframe, pzframe]

    xbeta = (vvec[1]/vvec[0])
    ybeta = (vvec[2]/vvec[0])
    zbeta = (vvec[3]/vvec[0])
    sbeta = xbeta**2 + ybeta**2 + zbeta**2
    gama = 1/math.sqrt((1-sbeta))

    l00 = gama
    l01 = -gama * xbeta
    l02 = -gama * ybeta
    l03 = -gama * zbeta

    l10 = l01
    l11 = 1 + ((gama - 1) * (xbeta**2) / sbeta)
    l12 = (gama - 1) * xbeta * ybeta / sbeta
    l13 = (gama - 1) * xbeta * zbeta / sbeta

    l20 = l02
    l21 = l12
    l22 = 1 + ((gama - 1) * (ybeta**2) / sbeta)
    l23 = (gama - 1) * ybeta * zbeta / sbeta

    l30 = l03
    l31 = l13
    l32 = l23
    l33 = 1 + ((gama - 1) * (zbeta**2) / sbeta)

    Ltrans = np.array([[l00, l01, l02, l03], [l10, l11, l12, l13], [l20, l21, l22, l23], [l30, l31, l32, l33]])

    if meter == 1:      #to rest frame of Higgs
        p4trans = (Ltrans.dot(p4.transpose())).transpose()
    else:
        p4trans = ((np.linalg.inv(Ltrans)).dot(p4.transpose())).transpose()
    #print(p4trans)

    return p4trans

def coord_rot(vec, fi, teta):

    E = vec[0]
    x = vec[1] * math.sin(teta) * math.cos(fi) + vec[2] * math.sin(teta) * math.sin(fi) + vec[3] * math.cos(teta)
    y = -vec[1] * math.sin(fi) + vec[2] * math.cos(fi)
    z = -vec[1] * math.cos(teta) * math.cos(fi) - vec[2] * math.cos(teta) * math.sin(fi) + vec[3] * math.sin(teta)
    vecrot = np.array([E, x, y, z])

    return vecrot

def projekt(p4):

    vecsiz = p4[1]
    p0 = math.sqrt(p4[0]**2 + p4[1]**2)
    fi = p4[2]
    teta = p4[3]
    x = vecsiz * math.sin(teta) * math.cos(fi)
    y = vecsiz * math.sin(teta) * math.sin(fi)
    z = vecsiz * math.cos(teta)
    projek = np.array([p0, x, y, z])

    return projek
