import decay
import matr_mult
import math
import random
import random_gauss
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

mz = 91
mmiu = 0.11
n = 20000
veikia = np.array([])
for x in range(n):
    m = random_gauss.rand_gauss(125, 0.004)
    hold = np.array(decay.newdecay(m, mz, 0, mmiu))
    inpart1 = np.array(hold[0])
    inpart2 = np.array(hold[1])   #intermediate particle data as [mass, momentum, phi, teta]
    inpart1 = np.array(matr_mult.projekt(inpart1))
    inpart2 = np.array(matr_mult.projekt(inpart2))
    inpart1rest = np.array(matr_mult.lorentz(inpart1, inpart1, 0))
    inpart2rest = np.array(matr_mult.lorentz(inpart2, inpart2, 0))
    hold = np.array(decay.newdecay(inpart1rest[0], 0, mmiu, 0))
    part11 = np.array(hold[0])
    part12 = np.array(hold[1])
    hold = np.array(decay.newdecay(inpart2rest[0], 0, mmiu, 0))
    part21 = np.array(hold[0])
    part22 =np.array(hold[1])    #particle data

    ###

    p41 = np.array(matr_mult.projekt(part11))
    p42 = np.array(matr_mult.projekt(part12))
    p43 = np.array(matr_mult.projekt(part21))
    p44 = np.array(matr_mult.projekt(part22))

    p41 = np.array(matr_mult.lorentz(p41, inpart1, 1))
    p42 = np.array(matr_mult.lorentz(p42, inpart1, 1))
    p43 = np.array(matr_mult.lorentz(p43, inpart2, 1))
    p44 = np.array(matr_mult.lorentz(p44, inpart2, 1))
    pagaliau = p41[0] + p42[0] + p43[0] + p44[0]
    veikia = np.append(veikia, pagaliau)

wh = np.ones_like(veikia)/float(len(veikia))
n, bins, patches = plt.hist(veikia, 70, weights=wh, edgecolor='black')
plt.xlabel('Energy')
plt.ylabel('Probability')
plt.title('Sum of Particle energies')
plt.show()
