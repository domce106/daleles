import random
import math
import random_gauss
import numpy as np

def decay(m):
    phg = np.array([m,0,0,0])   #higgs 4vector
    fi1 = random.random()*2*math.pi     #angle phi
    teta1 = random.random()*math.pi     #angle teta
    mm1 = 0   #generate random mass of mediator
    mm2 = 0
    temp = 0
    while mm1==0:
        temp = random.random()
        mm1 = temp*m
    while mm2==0:
        temp = random.random()
        mm2 = temp*(m-mm1)
    mom1 = math.sqrt((((m**2 + mm1**2 - mm2**2)**2) / (4*(m**2))) - mm1**2)      #mediator momentum
    ret1 = np.array([mm1,  mom1, fi1, teta1])
    ret2 = np.array([mm2, -mom1, fi1, teta1])
    ret = np.array([ret1, ret2])
    return ret

def newdecay(m, mass1, mass2, mass3):
    phg = np.array([m,0,0,0])   #higgs 4vector
    fi1 = random.random()*2*math.pi     #angle phi
    teta1 = random.random()*math.pi     #angle teta
    mm1 = 0   #generate random mass of mediator
    mm2 = 0
    temp = 0

    if mass1 == 0 and mass2 == 0:
        while mm1 == 0:
            temp = random.random()
            mm1 = temp*m
        while mm2==0:
            temp = random.random()
            mm2 = temp*(m-mm1)

    elif mass1 != 0 and mass2 == 0:
        mm1 = mass1
        while mm2 == 0 or mm2 < (2 * mass3):
            temp = random.random()
            mm2 = temp*(m-mm1)

    elif mass1 == 0 and mass2 != 0:
        mm1 = mass2
        mm2 = mass2

    mom1 = math.sqrt((((m**2 + mm1**2 - mm2**2)**2) / (4*(m**2))) - mm1**2)      #mediator momentum
    ret1 = np.array([mm1,  mom1, fi1, teta1])
    ret2 = np.array([mm2, -mom1, fi1, teta1])
    ret = np.array([ret1, ret2])
    return ret
