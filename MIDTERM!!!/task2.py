import csv
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import numpy as np

with open('csvfailas.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile)
    spamwriter.writerow(['91'] + ['625'])
    spamwriter.writerow(['125', '350'])
    spamwriter.writerow(['182'] + ['1770'])

width = 1
dis1 = np.full(625, 91)
dis2 = np.full(350, 125)
dis3 = np.full(1770, 182)
n, bins, patches = plt.hist(dis1, width, color = 'green')
n, bins, patches = plt.hist(dis2, width, color = 'blue')
n, bins, patches = plt.hist(dis3, width, color = 'red')
plt.xlabel('m, GeV')
plt.ylabel('n, events')
plt.title('Plot')
plt.show()
