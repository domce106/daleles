import random
import math
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

n = 20000
width = 75
dis1 = np.array([])
dis2 = np.array([])
dis3 = np.array([])
for i in range(n):
    dis1 = np.append(dis1, random.gauss(91, 2.5))
    dis2 = np.append(dis2, random.gauss(125, 3))
    dis3 = np.append(dis3, random.gauss(182, 20))

plt.figure(1)
wh = np.ones_like(dis1)/float(len(dis1))
n, bins, patches = plt.hist(dis1, width, color = 'green')
n, bins, patches = plt.hist(dis2, width, color = 'blue')
n, bins, patches = plt.hist(dis3, width, color = 'red')
plt.xlabel('m, GeV')
plt.ylabel('n, events')
plt.title('Plot')
#plt.show()

plt.figure(2)

wh1 = np.ones_like(dis1)*float(625)/float(len(dis1))
wh2 = np.ones_like(dis2)*float(350)/float(len(dis2))
wh3 = np.ones_like(dis3)*float(1770)/float(len(dis3))
n, bins, patches = plt.hist(dis1, width, weights=wh1, color = 'green')
n, bins, patches = plt.hist(dis2, width, weights=wh2, color = 'blue')
n, bins, patches = plt.hist(dis3, width, weights=wh3, color = 'red')
plt.xlabel('m, GeV')
plt.ylabel('n, events')
plt.title('Plot')
plt.show()
print(wh1)
